#ifndef CAMERA_HPP_
#define CAMERA_HPP_
/////////////////////////////////////

#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>

#ifdef MACOSX
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include "Vector3.hpp"
#include "Frustum.hpp"

using namespace std;

struct Camera
{
  Vector3 position; //Camera position
  Vector3 look; //Position camera looks at

  Vector3 up; //Camera up vector
  Vector3 forward; //Camera forward vector
  Vector3 right; //Camera right vector

  GLdouble speed; //Stores camera speed for movement
  GLdouble rotateSpeed; //Stores camera rotation speed

  ViewingFrustum viewingFrustum; //Viewing frustum
  ////////////////////////////////////////////////////////////////////////////
  //Constructors
  Camera() : position(0, 0, 0), look(1, 0, 0), up(0, 1, 0), speed(50), rotateSpeed(0.1)
  {
    calculateForward();
    calculateRight();
    calculateUp();
  }

  Camera(Vector3 pos, Vector3 look, Vector3 up, GLdouble speed, GLdouble rotateSpeed) : position(pos), look(look), up(up), speed(speed), rotateSpeed(rotateSpeed)
  {
    calculateForward();
    calculateRight();
    calculateUp();
  }
  ////////////////////////////////////////////////////////////////////////////
  //Returns vector encoded as string
  string toString()
  {
    ostringstream strStream;
    strStream << "Camera {" << endl
    << "Position:" << position.toString() << "," << endl
    << " Look:" << look.toString() << "," << endl
    << " Up:" << up.toString() << "," << endl
    << " Forward:" << forward.toString() << "," << endl
    << " Right:" << right.toString() << endl
    << "}";
    return strStream.str();
  }

  //Updates viewing frustum planes
  void updateViewingFrustum(double fov, double ratio, double nearDist, double farDist)
  {
    //Defines width and height of near and far planes
    double Hnear = 2 * tan((M_PI / 180.0) * fov * 0.5) * nearDist;
    double Wnear = Hnear * ratio;
    double Hfar = 2 * tan((M_PI / 180.0) * fov * 0.5) * farDist;
    double Wfar = Hfar * ratio;
    //Centre position of near and far planes
    Vector3 farCentre = position + forward * farDist;
    Vector3 nearCentre = position + forward * nearDist;

    //Defines vertex of viewing frustum
    Vector3 farTopLeft = farCentre + (up * Hfar * 0.5) - (right * Wfar * 0.5);
    Vector3 farTopRight = farCentre + (up * Hfar * 0.5) + (right * Wfar * 0.5);
    Vector3 farBottomLeft = farCentre - (up * Hfar * 0.5) - (right * Wfar * 0.5);
    Vector3 farBottomRight = farCentre - (up * Hfar * 0.5) + (right * Wfar * 0.5);

    Vector3 nearTopLeft = nearCentre + (up * Hnear * 0.5) - (right * Wnear * 0.5);
    Vector3 nearTopRight = nearCentre + (up * Hnear * 0.5) + (right * Wnear * 0.5);
    Vector3 nearBottomLeft = nearCentre - (up * Hnear * 0.5) - (right * Wnear * 0.5);
    Vector3 nearBottomRight = nearCentre - (up * Hnear * 0.5) + (right * Wnear * 0.5);

    //Defines viewing frustum planes
    viewingFrustum.setPlane(viewingFrustum.NEARP, nearTopLeft, nearTopRight, nearBottomRight);
    viewingFrustum.setPlane(viewingFrustum.FARP, farTopRight, farTopLeft, farBottomLeft);

    viewingFrustum.setPlane(viewingFrustum.TOP, nearTopRight, nearTopLeft, farTopLeft);
    viewingFrustum.setPlane(viewingFrustum.BOTTOM, nearBottomLeft, nearBottomRight, farBottomRight);

    viewingFrustum.setPlane(viewingFrustum.LEFT, nearTopLeft, nearBottomLeft, farBottomLeft);
    viewingFrustum.setPlane(viewingFrustum.RIGHT, nearBottomRight, nearTopRight, farBottomRight);
  }

  //Performs gluLookAt for this camera
  void gluLook()
  {
    gluLookAt(position.x, position.y, position.z,
          look.x, look.y, look.z,
          up.x, up.y, up.z);
  }

  //Moves camera forwards by distance
  void moveForward(GLdouble distance) { position += forward * distance; look += forward * distance; viewingFrustum.translate(forward * distance); }
  //Moves camera backwards by distance
  void moveBackward(GLdouble distance) { moveForward(-distance); }

  //Moves camera up by distance
  void moveUp(GLdouble distance) { position += up * distance; look += up * distance; viewingFrustum.translate(up * distance); }
  //Moves camera down by distance
  void moveDown(GLdouble distance) { moveUp(-distance); }

  //Moves camera right by distance
  void moveRight(GLdouble distance) { position += right * distance; look += right * distance; viewingFrustum.translate(right * distance); }
  //Moves camera left by distance
  void moveLeft(GLdouble distance) { moveRight(-distance); }

  //Rotates forward, right, up, and look vectors around axis by radians
  void rotateAround(GLdouble radians, Vector3 axis)
  { //Uses quaternions
    GLdouble q0 = cos(radians / 2);
    GLdouble q1 = sin(radians / 2) * axis.x;
    GLdouble q2 = sin(radians / 2) * axis.y;
    GLdouble q3 = sin(radians / 2) * axis.z;

    //Rotation matrix as individual variables
    GLdouble topLeft = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3;
    GLdouble topMid = 2 * (q1 * q2 - q0 * q3);
    GLdouble topRight = 2 * (q1 * q3 + q0 * q2);

    GLdouble midLeft = 2 * (q2 * q1 + q0 * q3);
    GLdouble midMid = q0 * q0 - q1 * q1 + q2 * q2 - q3 * q3;
    GLdouble midRight = 2 * (q2 * q3 - q0 * q1);

    GLdouble lowLeft = 2 * (q3 * q1 - q0 * q2);
    GLdouble lowMid = 2 * (q3 * q2 + q0 * q1);
    GLdouble lowRight = q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3;

    //Rotate forward vector
    Vector3 old_forward(forward);
    forward.x = old_forward.x * topLeft + old_forward.y * topMid + old_forward.z * topRight;
    forward.y = old_forward.x * midLeft + old_forward.y * midMid + old_forward.z * midRight;
    forward.z = old_forward.x * lowLeft + old_forward.y * lowMid + old_forward.z * lowRight;

    //Rotate right vector
    Vector3 old_right(right);
    right.x = old_right.x * topLeft + old_right.y * topMid + old_right.z * topRight;
    right.y = old_right.x * midLeft + old_right.y * midMid + old_right.z * midRight;
    right.z = old_right.x * lowLeft + old_right.y * lowMid + old_right.z * lowRight;

    //Rotate up vector
    Vector3 old_up(up);
    up.x = old_up.x * topLeft + old_up.y * topMid + old_up.z * topRight;
    up.y = old_up.x * midLeft + old_up.y * midMid + old_up.z * midRight;
    up.z = old_up.x * lowLeft + old_up.y * lowMid + old_up.z * lowRight;

    //If forward vector has changed then update look vector
    if (forward != old_forward)
      look = position + forward;
  }

  //Rotates camera around up vector
  void rotateAroundUp(GLdouble radians)
  { // Uses quaternions
    rotateAround(radians, up);
  }

  //Rotates camera around forward vector
  void rotateAroundForward(GLdouble radians)
  { // Uses quaternions
    rotateAround(radians, forward);
  }

  //Rotates camera around right vector
  void rotateAroundRight(GLdouble radians)
  { // Uses quaternions
    rotateAround(radians, right);
  }

  private:
  //Calculates forward vector
  void calculateForward() { forward = look - position; forward.normalise(); }

  //Calculates right vector as cross product of forward and up vectors
  void calculateRight()
  {
    right.x = forward.y * up.z - forward.z * up.y;
    right.y = forward.z * up.x - forward.x * up.z;
    right.z = forward.x * up.y - forward.y * up.x;
    right.normalise();
  }

  //Calculates up vector as cross product of right and forward vectors
  void calculateUp()
  {
    up.x = right.y * forward.z - right.z * forward.y;
    up.y = right.z * forward.x - right.x * forward.z;
    up.z = right.x * forward.y - right.y * forward.x;
    up.normalise();
  }
};

/////////////////////////////////////
#endif //CAMERA_HPP_
