#ifndef VECTOR3_HPP_
#define VECTOR3_HPP_
/////////////////////////////////////

#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>

#ifdef MACOSX
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

using namespace std;

struct Vector3
{
  GLdouble x, y, z;
  ////////////////////////////////////////////////////////////////////////////
  //Constructors
  Vector3(const Vector3& other) : x(other.x), y(other.y), z(other.z) {}
  Vector3(GLdouble xVal, GLdouble yVal, GLdouble zVal) : x(xVal), y(yVal), z(zVal) {}
  Vector3() : x(0), y(0), z(0) {} //Default
  ////////////////////////////////////////////////////////////////////////////
  //Overloaded operators
  void operator=(Vector3 rhs)
  {
    x = rhs.x;
    y = rhs.y;
    z = rhs.z;
  }

  void operator+=(Vector3 rhs)
  {
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
  }

  void operator-=(Vector3 rhs)
  {
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
  }

  void operator*=(double rhs)
  {
    x *= rhs;
    y *= rhs;
    z *= rhs;
  }

  Vector3 operator*(double rhs) const
  {
    Vector3 result;
    result.x = x * rhs;
    result.y = y * rhs;
    result.z = z * rhs;
    return result;
  }

  Vector3 operator*(Vector3 rhs) const
  {
    Vector3 result;
    result.x = x * rhs.x;
    result.y = y * rhs.y;
    result.z = z * rhs.z;
    return result;
  }

  Vector3 operator/(int rhs) const
  {
    Vector3 result;
    result.x = x / rhs;
    result.y = y / rhs;
    result.z = z / rhs;
    return result;
  }

  Vector3 operator+(Vector3 rhs) const
  {
    Vector3 result(*this);
    result.x += rhs.x;
    result.y += rhs.y;
    result.z += rhs.z;
    return result;
  }

  Vector3 operator-(Vector3 rhs) const
  {
    Vector3 result(*this);
    result.x -= rhs.x;
    result.y -= rhs.y;
    result.z -= rhs.z;
    return result;
  }

  Vector3 operator-() const
  {
    return Vector3(-this->x, -this->y, -this->z);
  }

  bool operator==(Vector3 rhs) const
  {
    return (x == rhs.x && y == rhs.y && z == rhs.z);
  }

  bool operator!=(Vector3 rhs) const
  {
    return !(*this == rhs);
  }
  ////////////////////////////////////////////////////////////////////////////
  //Returns vector encoded as string
  string toString()
  {
    ostringstream strStream;
    strStream << "(" << x << "," << y << "," << z << ")";
    return strStream.str();
  }

  //Returns magnitude of vector
  GLdouble magnitude()
  {
    return sqrt(x * x + y * y + z * z);
  }

  //Normalises and returns vector
  Vector3 normalise()
  {
    GLdouble sum = magnitude();
    x /= sum;
    y /= sum;
    z /= sum;
    return *this;
  }

  double dotProduct(Vector3 rhs)
  {
    return this->x * rhs.x + this->y * rhs.y + this->z * rhs.z;
  }

  Vector3 crossProduct(Vector3 rhs)
  {
    Vector3 result;
    result.x = this->y * rhs.z - this->z * rhs.y;
    result.y = this->z * rhs.x - this->x * rhs.z;
    result.z = this->x * rhs.y - this->y * rhs.x;
    return result;
  }

  //Rotates vector around X-axis by radians and returns.
  //If update is true then changes the value of vector else creates new
  Vector3 rotateAroundX(GLdouble radians, bool update = false)
  {
    if (update)
    {
    GLdouble old_x = x, old_y = y, old_z = z;
    x = old_x;
    y = old_y * cos(radians) - old_z * sin(radians);
    z = old_y * sin(radians) + old_z * cos(radians);
    return *this;
    }
    else
    {
    Vector3 result(*this);
    result.x = x;
    result.y = y * cos(radians) - z * sin(radians);
    result.z = y * sin(radians) + z * cos(radians);
    return result;
    }
  }

  //Rotates vector around Y-axis by radians and returns.
  //If update is true then changes the value of vector else creates new
  Vector3 rotateAroundY(GLdouble radians, bool update = false)
  {
    if (update)
    {
    GLdouble old_x = x, old_y = y, old_z = z;
    x = old_x * cos(radians) + old_z * sin(radians);
    y = old_y;
    z = -old_x * sin(radians) + old_z * cos(radians);
    return *this;
    }
    else
    {
    Vector3 result(*this);
    result.x = x * cos(radians) + z * sin(radians);
    result.y = y;
    result.z = -x * sin(radians) + z * cos(radians);
    return result;
    }
  }

  //Rotates vector around Z-axis by radians and returns.
  //If update is true then changes the value of vector else creates new
  Vector3 rotateAroundZ(GLdouble radians, bool update = false)
  {
    if (update)
    {
    GLdouble old_x = x, old_y = y, old_z = z;
    x = old_x * cos(radians) - old_y * sin(radians);
    y = old_x * sin(radians) + old_y * cos(radians);
    z = old_z;
    return *this;
    }
    else
    {
    Vector3 result(*this);
    result.x = x * cos(radians) - y * sin(radians);
    result.y = x * sin(radians) + y * cos(radians);
    result.z = z;
    return result;
    }
  }
};

/////////////////////////////////////
#endif //VECTOR3_HPP_
