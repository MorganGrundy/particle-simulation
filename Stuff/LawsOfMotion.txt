Classical mechanics, not quantum

Newton's laws of motion:
1st - Object remains at rest or continues to move at constant velocity, unless acted upon by a force
2nd - The vector sum of the forces F on an object is equal to the mass m of that object multiplied by the acceleration a of the object; F = m * a
3rd - When one body exerts a force on a second body, the second body simultaneously exerts an equal and opposite force on the first body

Euler's laws of motion:
1st - Linear momentum of a body, p is equal to the product of the mass of the body m and the velocity of its centre of mass v_cm;   p = m * v_cm
2nd - No angular momentum, so not applicable

Cauchy law of motion:
- Describes momentum transport in continuum (continuous mass, not discrete particles). Hence not applicable as modelled discrete particles.

Kepler's laws of planetary motion:
- Not applicable

General relativity:
- Not applicable

Special relativity:
- Not applicable

------------------------------------------------------------------------------------------------------------
Newton's 3rd:
During collisions, two particles swap a component of velocity across the collision vector.
Acceleration = Change of velocity / time
Hence accelerations are equal and opposite.
Particle masses are identical, hence by Newton's 2nd law F = m * a the forces are equal and opposite.

Euler's 1st:
Momentum is conserved during collisions.
Momentum before = Momentum after

Gravity is simulated as a constant acceleration across all space-time with no source, hence no equal and opposite force for Newton's 3rd

------------------------------------------------------------------------------------------------------------
Every tick:
- New particles are added
- Particles exceeding their lifetime are removed
- Accelerations are applied to particle velocity
- Handles collisions
- Velocity is applied to particle position
- Particle life remaining updated

Ticksteps are fixed.

Collisions are modelled as perfectly elastic, hence KE is also conserved during collisions.
From tests it can be seen that KE is not conserved exactly, there is a very small variance due to precision & accuracy of C++ math functions and data types.
This also applies to conservation of momentum.
Such inaccuracies may result in different forces on each particle in a collision, hence affecting Newton's 3rd law.
