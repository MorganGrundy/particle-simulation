#ifndef FRUSTUM_HPP_
#define FRUSTUM_HPP_
/////////////////////////////////////

#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>

#ifdef MACOSX
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include "Vector3.hpp"
#include "Plane.hpp"

using namespace std;

struct ViewingFrustum
{
  enum
  {
    TOP = 0, BOTTOM, LEFT, RIGHT, NEARP, FARP
  };

  Plane pl[6];
  ////////////////////////////////////////////////////////////////////////////
  //Constructors
  ViewingFrustum() {} //Default
  ////////////////////////////////////////////////////////////////////////////
  //Overloaded operators

  ////////////////////////////////////////////////////////////////////////////
  //Sets definition of plane i from normal and position
  void setPlane(int i, Vector3 normal, Vector3 pos)
  {
    pl[i].redefine(normal, pos);
  }

  //Sets definition of plane i from three points
  void setPlane(int i, Vector3 p0, Vector3 p1, Vector3 p2)
  {
    pl[i].redefine(p0, p1, p2);
  }

  //Translate the viewing frustum by given vector
  ViewingFrustum translate(Vector3 t)
  {
    for (int i = TOP; i < FARP; ++i)
      pl[i].translate(t);
    return *this;
  }

  //Checks if sphere intersects with frustum
  bool sphereInFrustum(Vector3 pos, double radius)
  {
    double dist;
    for (int i = TOP; i < FARP; ++i)
    {
      dist = pl[i].distToPoint(pos);
      if (dist < -radius)
        return false;
    }
    return true;
  }
};

/////////////////////////////////////
#endif //FRUSTUM_HPP_
