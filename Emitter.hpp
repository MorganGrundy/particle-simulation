#ifndef EMITTER_HPP_
#define EMITTER_HPP_
/////////////////////////////////////

#include <vector>
#include <string>
#include <cmath>

#include "Vector3.hpp"
#include "Particle.hpp"

#ifdef MACOSX
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

///////////////////////////////////////////////

//Return random double within range [-1,1]
double myRand()
{
  return (rand() / ((double) RAND_MAX / 2)) - 1;
}

///////////////////////////////////////////////

struct Emitter
{
  Vector3 position; //Position of emitter
  ParticleType *type; //Type of particle emitter emits
  //Mean and variance of particle emission rate, per second
  double meanParticleEmitRate, particleEmitRateVariance;
  //Mean and variance of particle velocity
  Vector3 meanParticleVelocity, particleVelocityVariance;

  ////////////////////////////////////////////////////////////////////////////
  //Constructors
  Emitter() : position(0,0,0), type(&defaultParticleType), meanParticleEmitRate(1), particleEmitRateVariance(1), meanParticleVelocity(4,10,0), particleVelocityVariance(2,5,0)
  {}
  Emitter(Vector3 pos, ParticleType *particleType, double meanParticleEmitRate, double particleEmitRateVariance, Vector3 meanParticleVelocity, Vector3 particleVelocityVariance) : position(pos), type(particleType), meanParticleEmitRate(meanParticleEmitRate), particleEmitRateVariance(particleEmitRateVariance), meanParticleVelocity(meanParticleVelocity), particleVelocityVariance(particleVelocityVariance)
  {}
  ////////////////////////////////////////////////////////////////////////////
  void addParticles(ParticleGroup *particleGroup, double delta);
};

struct PointEmitter : Emitter
{
  double meanParticleSpeed, particleSpeedVariance;
  ////////////////////////////////////////////////////////////////////////////
  //Constructors
  PointEmitter() : Emitter(), meanParticleSpeed(5), particleSpeedVariance(2)
  {}
  PointEmitter(Vector3 pos, ParticleType *particleType, double meanParticleEmitRate, double particleEmitRateVariance, double meanParticleSpeed, double particleSpeedVariance) : Emitter(pos, particleType, meanParticleEmitRate, particleEmitRateVariance, Vector3(0,0,0), Vector3(0,0,0)), meanParticleSpeed(meanParticleSpeed), particleSpeedVariance(particleSpeedVariance)
  {}
  ////////////////////////////////////////////////////////////////////////////
  //Rotates forward, right, up, and look vectors around axis by radians
  void rotateAround(GLdouble radians, Vector3 axis, Vector3& vectorToRot)
  { //Uses quaternions
    GLdouble q0 = cos(radians / 2);
    GLdouble q1 = sin(radians / 2) * axis.x;
    GLdouble q2 = sin(radians / 2) * axis.y;
    GLdouble q3 = sin(radians / 2) * axis.z;

    //Rotation matrix as individual variables
    GLdouble topLeft = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3;
    GLdouble topMid = 2 * (q1 * q2 - q0 * q3);
    GLdouble topRight = 2 * (q1 * q3 + q0 * q2);

    GLdouble midLeft = 2 * (q2 * q1 + q0 * q3);
    GLdouble midMid = q0 * q0 - q1 * q1 + q2 * q2 - q3 * q3;
    GLdouble midRight = 2 * (q2 * q3 - q0 * q1);

    GLdouble lowLeft = 2 * (q3 * q1 - q0 * q2);
    GLdouble lowMid = 2 * (q3 * q2 + q0 * q1);
    GLdouble lowRight = q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3;

    //Rotate forward vector
    Vector3 oldVector(vectorToRot);
    vectorToRot.x = oldVector.x * topLeft + oldVector.y * topMid + oldVector.z * topRight;
    vectorToRot.y = oldVector.x * midLeft + oldVector.y * midMid + oldVector.z * midRight;
    vectorToRot.z = oldVector.x * lowLeft + oldVector.y * lowMid + oldVector.z * lowRight;
  }

  Vector3 rotateSpeed(double speed)
  {
    Vector3 velocity(speed, 0, 0);
    rotateAround(M_PI * myRand(), Vector3(0,1,0), velocity);
    rotateAround(M_PI * myRand(), Vector3(0,0,1), velocity);
    return velocity;
  }

  //Generates new particles in the given particle group, unless max reached
  void addParticles(ParticleGroup *particleGroup, double delta)
  {
    if (type == NULL)
      return;

    int noOfParticles = (meanParticleEmitRate + particleEmitRateVariance * myRand()) * delta;

    for (int i = 0; i < noOfParticles && (*particleGroup).activeParticles < (*particleGroup).maxParticles; ++i)
    {
      Particle *curParticle = &((*particleGroup).particles[(*particleGroup).activeParticles]);
      (*curParticle).pos = position;
      (*curParticle).velocity = rotateSpeed(meanParticleSpeed + particleSpeedVariance * myRand());
      (*curParticle).lifeLeft = type->lifetime;

      //Check for collisions with other particles
      for (int otherPart = ++(*particleGroup).activeParticles - 1; otherPart > 0; --otherPart)
      {
        curParticle->collisionCheck(&((*particleGroup).particles[otherPart]), type->radius);
      }
    }
  }
};

struct GeometryEmitter : Emitter
{
  Vector3 size;
  ////////////////////////////////////////////////////////////////////////////
  //Constructors
  GeometryEmitter() : Emitter(), size(Vector3(5,5,5))
  {}
  GeometryEmitter(Vector3 pos, ParticleType *particleType, double meanParticleEmitRate, double particleEmitRateVariance, Vector3 meanParticleVelocity, Vector3 particleVelocityVariance, Vector3 size) : Emitter(pos, particleType, meanParticleEmitRate, particleEmitRateVariance, meanParticleVelocity, particleVelocityVariance), size(size / 2)
  {}
  ////////////////////////////////////////////////////////////////////////////
  //Generates new particles in the given particle group, unless max reached
  void addParticles(ParticleGroup *particleGroup, double delta)
  {
    if (type == NULL)
      return;

    int noOfParticles = (meanParticleEmitRate + particleEmitRateVariance * myRand()) * delta;

    for (int i = 0; i < noOfParticles && (*particleGroup).activeParticles < (*particleGroup).maxParticles; ++i)
    {
      Particle *curParticle = &((*particleGroup).particles[(*particleGroup).activeParticles]);
      (*curParticle).pos = position + size * Vector3(myRand(), myRand(), myRand());
      (*curParticle).velocity = meanParticleVelocity + particleVelocityVariance * Vector3(myRand(), myRand(), myRand());
      (*curParticle).lifeLeft = type->lifetime;

      //Check for collisions with other particles
      for (int otherPart = ++(*particleGroup).activeParticles - 1; otherPart > 0; --otherPart)
      {
        curParticle->collisionCheck(&((*particleGroup).particles[otherPart]), type->radius);
      }
    }
  }
};

/////////////////////////////////////
#endif //EMITTER_HPP_
