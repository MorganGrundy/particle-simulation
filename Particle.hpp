#ifndef PARTICLE_HPP_
#define PARTICLE_HPP_
/////////////////////////////////////

#include <vector>
#include <string>
#include <cmath>

#ifdef MACOSX
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include "Vector3.hpp"
#include "Camera.hpp"
#include "Plane.hpp"

Vector3 GRAVITY(0, -9.80665, 0);
Vector3 WIND(0, 0, 0);

GLuint PARTICLE = 1;

struct ParticleType
{
  string name; //Particle type name
  double mass; //Mass of particle
  double lifetime; //Particle lifetime
  GLdouble radius; //Particle radius
  bool hasCollision; //If particle can collide

  ////////////////////////////////////////////////////////////////////////////
  //Constructors
  ParticleType() : name("Default"), mass(1.0), lifetime(10.0), radius(1.0)
  {}

  ParticleType(string name, double mass, double lifetime, GLdouble radius) : name(name), mass(mass), lifetime(lifetime), radius(radius)
  {}
  ////////////////////////////////////////////////////////////////////////////
} defaultParticleType;

struct Particle
{
  Vector3 pos; //Particle position
  Vector3 velocity; //Particle velocity
  double lifeLeft; //Lifetime remaining for particle
  double timeToCollision; //Time until collision
  Particle* collideParticle; //Particle that will collide with, if null no collision
  ////////////////////////////////////////////////////////////////////////////
  //Constructors
  Particle() : pos(0,0,0), velocity(0,0,0), lifeLeft(defaultParticleType.lifetime)
  {}
  Particle(Vector3 pos, Vector3 velocity, double lifetime) : pos(pos), velocity(velocity), lifeLeft(lifetime)
  {}
  ////////////////////////////////////////////////////////////////////////////
  //Checks for a collision with given particle
  //Treats moving sphere/moving sphere as ray/static sphere
  //Time of collision = (-b +- sqrt(b^2 - 4ac)) / 2a
  void collisionCheck(Particle* target, double radius)
  {
    //Direction + velocity of ray
    Vector3 partABVel(this->velocity - target->velocity);

    //a
    double collisionA = pow(partABVel.x, 2) + pow(partABVel.y, 2) + pow(partABVel.z, 2);

    //b
    double collisionB = 2 * (partABVel.dotProduct(this->pos - target->pos));

    //c
    double collisionC = pow(this->pos.x - target->pos.x, 2) + pow(this->pos.y - target->pos.y, 2) + pow(this->pos.z - target->pos.z, 2) - pow(radius, 2);

    //b^2 - 4ac
    double discriminant = collisionB * collisionB - 4 * collisionA * collisionC;
    //If discriminant < 0 then no collisions
    //                = 0 then single collision, just ignore
    //                > 0 then two collisions
    if (discriminant > 0)
    {
      //Calculate two collision times
      double t0 = (-collisionB + sqrt(discriminant)) / (2 * collisionA);
      double t1 = (-collisionB - sqrt(discriminant)) / (2 * collisionA);
      //Only care about earliest (point that spheres collide)
      double t = (t0 > t1) ? t0 : t1;
      //Ignore collisions already occuring
      if (t > 0)
      {
        //Calculate minimum life left of two particles
        double minLife = (this->lifeLeft > target->lifeLeft) ? this->lifeLeft : target->lifeLeft;

        //If t happens in particle lifetime, and is sooner than any other collision with particles
        if (t < minLife && (this->collideParticle == NULL || this->timeToCollision > t) && (target->collideParticle == NULL || target->timeToCollision > t))
        {
          //Set collision for particles
          //cout << "Collision: " << t << endl;
          this->collideParticle = target;
          this->timeToCollision = t;

          target->collideParticle = this;
          target->timeToCollision = t;
        }
      }
    }
  }

  //Performs a perfectly elastic collision between this and other particle
  void collideWith(Particle* other)
  {
    //Calculate vector of collision
    Vector3 collisionVector(this->pos - other->pos);
    long double collisionMagnitude = collisionVector.magnitude();

    //Calculate angles between collision vector and x-axis
    //Alpha = rotation around y-axis
    //Beta = rotation around x-axis
    long double alpha = asin(collisionVector.z / -collisionMagnitude);
    long double beta = asin(collisionVector.x / (collisionMagnitude * cos(alpha)));

    //Rotate frame of reference so collision vector lays on x-axis
    this->velocity.rotateAroundZ(-beta, true);
    this->velocity.rotateAroundY(-alpha, true);
    other->velocity.rotateAroundZ(-beta, true);
    other->velocity.rotateAroundY(-alpha, true);
    //Swap x-components of particle velocities
    double tmp = this->velocity.x;
    this->velocity.x = other->velocity.x;
    other->velocity.x = tmp;
    //Rotate frame of reference so collision vector returns to original position
    this->velocity.rotateAroundY(alpha, true);
    this->velocity.rotateAroundZ(beta, true);
    other->velocity.rotateAroundY(alpha, true);
    other->velocity.rotateAroundZ(beta, true);
  }
};

struct ParticleGroup
{
  vector<Particle> particles; //Vector of particles
  ParticleType *type; //Pointer to particle type
  int maxParticles; //Max number of particles
  int activeParticles; //Current number of active particles

  long double maxDeltaMag;
  ////////////////////////////////////////////////////////////////////////////
  //Constructors
  ParticleGroup(int noOfParticles) : type(&defaultParticleType), maxParticles(noOfParticles), activeParticles(0)
  {
    particles.reserve(noOfParticles);
  }
  ParticleGroup(int noOfParticles, ParticleType *type) : type(type), maxParticles(noOfParticles), activeParticles(0)
  {
    for (int i = 0; i < noOfParticles; ++i)
    {
      Particle particle(Vector3(0,0,0), Vector3(0,0,0), type->lifetime);
      particles.push_back(particle);
    }
  }
  ////////////////////////////////////////////////////////////////////////////
  //Generates new particle, unless max particles reached
  void addParticle(Vector3 pos, Vector3 velocity)
  {
    if (activeParticles < maxParticles)
    {
      particles[activeParticles].pos = pos;
      particles[activeParticles].velocity = velocity;
      particles[++activeParticles].lifeLeft = type->lifetime;
    }
  }

  //Displays active particles
  void drawParticles(Camera* cam)
  {
    for (int i = 0 ; i < activeParticles; ++i)
    {
      //Viewing frustum culling
      //Only display spheres intersecting with viewing frustum
      if (cam->viewingFrustum.sphereInFrustum(particles[i].pos, type->radius))
      {
        Vector3 halfWidth = cam->right * type->radius;
        Vector3 halfHeight = cam->up * type->radius;
        glTranslatef(particles[i].pos.x, particles[i].pos.y, particles[i].pos.z);
        glBegin(GL_QUADS);
          glTexCoord2f(0, 0);
          glVertex3f(halfWidth.x + halfHeight.x, halfWidth.y + halfHeight.y, halfWidth.z + halfHeight.z);

          glTexCoord2f(0, 1);
          glVertex3f(-halfWidth.x + halfHeight.x, -halfWidth.y + halfHeight.y, -halfWidth.z + halfHeight.z);

          glTexCoord2f(1, 1);
          glVertex3f(-halfWidth.x - halfHeight.x, -halfWidth.y - halfHeight.y, -halfWidth.z - halfHeight.z);

          glTexCoord2f(1, 0);
          glVertex3f(halfWidth.x - halfHeight.x, halfWidth.y - halfHeight.y, halfWidth.z - halfHeight.z);
        glEnd();
        glTranslatef(-particles[i].pos.x, -particles[i].pos.y, -particles[i].pos.z);
      }
    }
  }

  //Updates particle positions
  void updateParticles(double delta)
  {
    for (int cur = 0; cur < activeParticles; ++cur)
    {
      //If particle life <= 0 then change to inactive (swap with last particle)
      if (particles[cur].lifeLeft <= 0)
      {
        iter_swap(particles.begin() + cur--, particles.begin() + --activeParticles);
      }
    }

    for (int cur = 0; cur < activeParticles; ++cur)
    {
      //If particle has a collision in future
      if (particles[cur].collideParticle != NULL)
      {
        //If collision not in this timestep then reduce time to collision by delta, else deal with collision
        if (particles[cur].timeToCollision > delta)
          particles[cur].timeToCollision -= delta;
        else //Handle collision
        {
          Particle* other = particles[cur].collideParticle;

          //Apply acceleration factors
          particles[cur].velocity += (WIND + GRAVITY) * delta;
          other->velocity += (WIND + GRAVITY) * delta;
          //Move to point of collision
          particles[cur].pos += particles[cur].velocity * particles[cur].timeToCollision;
          other->pos += other->velocity * particles[cur].timeToCollision;

          //Update velocity from collision
          particles[cur].collideWith(other);

          //Move remaining delta
          particles[cur].pos += particles[cur].velocity * (delta - particles[cur].timeToCollision);
          other->pos += other->velocity * (delta - particles[cur].timeToCollision);

          //Unlink particles
          particles[cur].collideParticle = NULL;
          other->collideParticle = NULL;
          //Prevents other particle from being moved twice
          other->timeToCollision = -1;

          particles[cur].collisionCheck(other, type->radius);
          other->collisionCheck(&(particles[cur]), type->radius);
        }
      }

      if (particles[cur].timeToCollision >= 0)
      {
        //Apply acceleration factors
        particles[cur].velocity += (WIND + GRAVITY) * delta;
        //Update particle position with velocity
        particles[cur].pos += particles[cur].velocity * delta;
      }
      else
        particles[cur].timeToCollision = 0;

      //Decrement particle life
      particles[cur].lifeLeft -= delta;
    }
  }
};

/////////////////////////////////////
#endif //PARTICLE_HPP_
