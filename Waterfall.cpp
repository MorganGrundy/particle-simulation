#include <iostream>
#include <ctime>
#include <iostream>
#include <fstream>

#ifdef MACOSX
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include "Vector3.hpp"
#include "Camera.hpp"
#include "Particle.hpp"
#include "Emitter.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

using namespace std;

#define INITIAL_WINDOW_WIDTH 1000.0
#define INITIAL_WINDOW_HEIGHT 1000.0

#define MAX_PARTICLES 1000000

#define MAX_FPS 60.0
#define MILLISECOND_BETWEEN_TICKS 1000 / MAX_FPS //Milliseconds between ticks
#define SECOND_BETWEEN_TICKS 1 / MAX_FPS //Seconds between ticks

// Display list for coordinate axis
GLuint axisList;
int AXIS_SIZE = 1000;
int axisEnabled = 1;

bool dataOutput = false;

//gluPerspective parameters, used for defining viewing frustum
double fov = 60;
double ratio = INITIAL_WINDOW_WIDTH / INITIAL_WINDOW_HEIGHT;
double nearDist = 1;
double farDist = 10000;

Camera cam(Vector3(-140, 110, 430), Vector3(100, 100, 100), Vector3(0, 1, 0), 10, 0.1);

ParticleGroup parts(MAX_PARTICLES);

GeometryEmitter originEmitter(Vector3(0,0,0), &defaultParticleType, 100, 10, Vector3(20, 60, 10), Vector3(0.1, 0.1, 0.1), Vector3(15, 15, 15));

GeometryEmitter otherEmitter(Vector3(200,0,0), &defaultParticleType, 100, 10, Vector3(-20, 60, 10), Vector3(0.1, 0.1, 0.1), Vector3(15, 15, 15));

//PointEmitter pointyEmitter(Vector3(50,-100,50), &defaultParticleType, 300, 1, 20, 10);

ofstream outputFile;

///////////////////////////////////////////////

//Return random double within range [0,1]
double myRandom()
{
  return rand() / (double) RAND_MAX;
}

///////////////////////////////////////////////

void drawString(void *font, double x, double y, string str)
{
  glRasterPos3f(x, y, 0);
  for (size_t i = 0; i < str.size(); ++i)
    glutBitmapCharacter(font, str[i]);
}

///////////////////////////////////////////////
//Time of last frame, initially program start time
double nextTick;
double lastFPSUpdate;

int frames = 0;
double fps = MAX_FPS;

void idle()
{
  //Limit FPS to MAX_FPS
  while (glutGet(GLUT_ELAPSED_TIME) < nextTick);

  //Calculate time till next frame as multiple of time per tick
  double tickStart;
  while (nextTick < (tickStart = glutGet(GLUT_ELAPSED_TIME)))
  {
    nextTick += MILLISECOND_BETWEEN_TICKS;
    //Create any new particles
    originEmitter.addParticles(&parts, SECOND_BETWEEN_TICKS);
    otherEmitter.addParticles(&parts, SECOND_BETWEEN_TICKS);
    //pointyEmitter.addParticles(&parts, SECOND_BETWEEN_TICKS);
    //Update particles
    parts.updateParticles(SECOND_BETWEEN_TICKS);

    if (dataOutput && outputFile.is_open())
      outputFile << glutGet(GLUT_ELAPSED_TIME) - tickStart << "," << fps << "," << parts.activeParticles << endl;
  }

  frames++;
  //Updates FPS count every second
  double timeSinceLastFPSUpdate = glutGet(GLUT_ELAPSED_TIME) - lastFPSUpdate;
  if (timeSinceLastFPSUpdate >= 1000.0)
  {
    fps = frames / (timeSinceLastFPSUpdate / 1000);
    frames = 0;
    lastFPSUpdate = glutGet(GLUT_ELAPSED_TIME);
  }
  glutPostRedisplay();
}

///////////////////////////////////////////////
//Display FPS on screen
void displayFPS()
{
  glColor3f(0, 0, 0);
  string fpsString = "FPS: " + to_string(fps);
  drawString(GLUT_BITMAP_9_BY_15, -0.98, 0.95, fpsString);
}

///////////////////////////////////////////////
//Display active particle count on screen
void displayParticleCount()
{
  glColor3f(0, 0, 0);
  string particleCountString = "Active particles: " + to_string(parts.activeParticles);
  drawString(GLUT_BITMAP_9_BY_15, -0.98, 0.90, particleCountString);
}

///////////////////////////////////////////////

void setOrthographicProjection()
{
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(-1, 1, -1, 1, -1, 1);
  glMatrixMode(GL_MODELVIEW);
}

void restorePerspectiveProjection()
{
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
}

///////////////////////////////////////////////

void display()
{
  glLoadIdentity();
  cam.gluLook();
  // Clear the screen
  glClear(GL_COLOR_BUFFER_BIT);

  // If enabled, draw coordinate axis
  if (axisEnabled)
  {
    glDisable(GL_TEXTURE_2D);
    glCallList(axisList);
    glEnable(GL_TEXTURE_2D);
  }

  //Display particles
  parts.drawParticles(&cam);

  setOrthographicProjection();

  glPushMatrix();
  glLoadIdentity();
  glDisable(GL_TEXTURE_2D);
  displayFPS();
  displayParticleCount();
  glEnable(GL_TEXTURE_2D);
  glPopMatrix();

  restorePerspectiveProjection();

  glutSwapBuffers();
}

///////////////////////////////////////////////

void keyboard(unsigned char key, int x, int y)
{
  if (key == 27)
    exit(0);

  if (key == 'w')
    cam.moveForward(cam.speed);
  else if (key == 's')
    cam.moveBackward(cam.speed);

  if (key == 'a')
    cam.moveLeft(cam.speed);
  else if (key == 'd')
    cam.moveRight(cam.speed);

  if (key == 'u')
    cam.moveUp(cam.speed);
  else if (key == 'j')
    cam.moveDown(cam.speed);

  if (key == 'v')
  {
    cam.rotateAroundUp(cam.rotateSpeed);
    cam.updateViewingFrustum(fov, ratio, nearDist, farDist);
  }
  else if (key == 'b')
  {
    cam.rotateAroundUp(-cam.rotateSpeed);
    cam.updateViewingFrustum(fov, ratio, nearDist, farDist);
  }

  if (key == 'f')
  {
    cam.rotateAroundForward(cam.rotateSpeed);
    cam.updateViewingFrustum(fov, ratio, nearDist, farDist);
  }
  else if (key == 'g')
  {
    cam.rotateAroundForward(-cam.rotateSpeed);
    cam.updateViewingFrustum(fov, ratio, nearDist, farDist);
  }

  if (key == 'r')
  {
    cam.rotateAroundRight(cam.rotateSpeed);
    cam.updateViewingFrustum(fov, ratio, nearDist, farDist);
  }
  else if (key == 't')
  {
    cam.rotateAroundRight(-cam.rotateSpeed);
    cam.updateViewingFrustum(fov, ratio, nearDist, farDist);
  }

  if (key == '#')
    axisEnabled = !axisEnabled;

  if (key == 'o')
    dataOutput = !dataOutput;

  if (key == 'c')
    cout << cam.toString() << endl;
}

///////////////////////////////////////////////

void reshape(int width, int height)
{
  glClearColor(0.9, 0.9, 0.9, 1.0);
  glViewport(0, 0, (GLsizei) width, (GLsizei) height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  ratio = (GLfloat) width / (GLfloat) height;
  gluPerspective(fov, ratio, nearDist, farDist);
  glMatrixMode(GL_MODELVIEW);
  //Updates viewing frustum with new camera properties
  cam.updateViewingFrustum(fov, ratio, nearDist, farDist);
}

///////////////////////////////////////////////

// Create a display list for drawing coord axis
void makeAxes()
{
  axisList = glGenLists(1);
  glNewList(axisList, GL_COMPILE);
    glLineWidth(2.0);
    glBegin(GL_LINES);
      glColor3f(1.0, 0.0, 0.0);       // X axis - red
      glVertex3f(0.0, 0.0, 0.0);
      glVertex3f(AXIS_SIZE, 0.0, 0.0);
      glColor3f(0.0, 1.0, 0.0);       // Y axis - green
      glVertex3f(0.0, 0.0, 0.0);
      glVertex3f(0.0, AXIS_SIZE, 0.0);
      glColor3f(0.0, 0.0, 1.0);       // Z axis - blue
      glVertex3f(0.0, 0.0, 0.0);
      glVertex3f(0.0, 0.0, AXIS_SIZE);
    glEnd();
  glEndList();
}

///////////////////////////////////////////////

void loadParticleTexture()
{
  unsigned int texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  int width, height, nrChannels;
  unsigned char *data = stbi_load("ParticleTexture.png", &width, &height, &nrChannels, 0);
  if (data)
  {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  }
  else
    cout << "Failed to load texture" << endl;

  stbi_image_free(data);
}

///////////////////////////////////////////////

void menuHandler(int val)
{
  switch(val)
  {
    case 20: exit(0); break;

    case 0: cout << "Current mean initial velocity: " << originEmitter.meanParticleVelocity.toString() << endl;
      cout << "Enter new mean initial velocity;" << endl;
      cout << "X: ";
      cin >> originEmitter.meanParticleVelocity.x;
      cout << "Y: ";
      cin >> originEmitter.meanParticleVelocity.y;
      cout << "Z: ";
      cin >> originEmitter.meanParticleVelocity.z;
      break;

    case 1: cout << "Current initial velocity variance: " << originEmitter.particleVelocityVariance.toString() << endl;
      cout << "Enter new initial velocity variance;" << endl;
      cout << "X: ";
      cin >> originEmitter.particleVelocityVariance.x;
      cout << "Y: ";
      cin >> originEmitter.particleVelocityVariance.y;
      cout << "Z: ";
      cin >> originEmitter.particleVelocityVariance.z;
      break;

    case 2: cout << "Current lifetime of particle: " << originEmitter.type->lifetime << endl;
      cout << "Enter new lifetime of particle: ";
      cin >> defaultParticleType.lifetime;
      break;

    case 3: cout << "Max number of particles: " << parts.maxParticles << endl;
      cout << "Enter new max number of particles: ";
      cin >> parts.maxParticles;
      break;

    case 4: cout << "Current mean particle emit rate: " << originEmitter.meanParticleEmitRate << endl;
      cout << "Enter new mean particle emit rate: ";
      double val;
      cin >> val;
      originEmitter.meanParticleEmitRate = val;
      otherEmitter.meanParticleEmitRate = val;
      break;

    case 5: cout << "Current particle emit rate variance: " << originEmitter.particleEmitRateVariance << endl;
      cout << "Enter new particle emit rate variance: ";
      cin >> originEmitter.particleEmitRateVariance;
      break;

    case 10: cout << "Current intensity of gravity: " << GRAVITY.toString() << endl;
      cout << "Enter new intensity of gravity;" << endl;
      cout << "X: ";
      cin >> GRAVITY.x;
      cout << "Y: ";
      cin >> GRAVITY.y;
      cout << "Z: ";
      cin >> GRAVITY.z;
      break;

    case 11: cout << "Current intensity of wind: " << WIND.toString() << endl;
      cout << "Enter new intensity of wind;" << endl;
      cout << "X: ";
      cin >> WIND.x;
      cout << "Y: ";
      cin >> WIND.y;
      cout << "Z: ";
      cin >> WIND.z;
      break;
  }
  nextTick = glutGet(GLUT_ELAPSED_TIME) + MILLISECOND_BETWEEN_TICKS;
  frames = 0;
  lastFPSUpdate = glutGet(GLUT_ELAPSED_TIME);
}

///////////////////////////////////////////////

void createMenu()
{
  glutCreateMenu(menuHandler);

  //Particle properties
  glutAddMenuEntry("Mean initial velocity", 0);
  glutAddMenuEntry("Initial velocity variance", 1);
  glutAddMenuEntry("Lifetime of particle", 2);
  glutAddMenuEntry("Max number of particles", 3);
  glutAddMenuEntry("Mean particle emit rate", 4);
  glutAddMenuEntry("Particle emit rate variance", 5);

  glutAddMenuEntry("Intensity of gravity", 10);
  glutAddMenuEntry("Intensity of wind", 11);

  glutAddMenuEntry("Exit", 20);

  glutAttachMenu(GLUT_RIGHT_BUTTON);
}

///////////////////////////////////////////////

void initGraphics(int argc, char *argv[])
{
  glutInit(&argc, argv);
  glutInitWindowSize(INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);
  glutInitWindowPosition(100, 100);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  glutCreateWindow("Particle Simulation - Waterfall");

  createMenu();
  loadParticleTexture();
  glutDisplayFunc(display);
  glutIdleFunc(idle);
  glutKeyboardFunc(keyboard);
  glutReshapeFunc(reshape);
  makeAxes();
}

/////////////////////////////////////////////////

int main(int argc, char **argv)
{
  if (argc > 1)
  {
    outputFile.open(argv[1]);
    outputFile << "Tick,FPS,Particle" << endl;
  }

  originEmitter.type = &defaultParticleType;
  otherEmitter.type = &defaultParticleType;
  //pointyEmitter.type = &defaultParticleType;
  srand(time(NULL));
  initGraphics(argc, argv);

  nextTick = glutGet(GLUT_ELAPSED_TIME) + MILLISECOND_BETWEEN_TICKS;
  lastFPSUpdate = glutGet(GLUT_ELAPSED_TIME);

  glutMainLoop();
  outputFile.close();
}
