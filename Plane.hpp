#ifndef PLANE_HPP_
#define PLANE_HPP_
/////////////////////////////////////

#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>

#ifdef MACOSX
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif

#include "Vector3.hpp"

using namespace std;

struct Plane
{
  Vector3 u, v, n, p;
  double D;
  ////////////////////////////////////////////////////////////////////////////
  //Constructors
  Plane() : u(), v(), n() {} //Default
  ////////////////////////////////////////////////////////////////////////////
  //Overloaded operators

  ////////////////////////////////////////////////////////////////////////////
  //Translate plane by given vector
  Plane translate(Vector3 t)
  {
    p += t;
    D = -n.dotProduct(p);
    return *this;
  }

  //Redefines plane with new normal and position
  Plane redefine(Vector3 norm, Vector3 pos)
  {
    n = norm;
    D = -n.dotProduct(pos);
    return *this;
  }

  //Redefines plane with new three points
  Plane redefine(Vector3 p0, Vector3 p1, Vector3 p2)
  {
    u = p2 - p0;
    v = p1 - p0;
    n = v.crossProduct(u).normalise();
    p = p0;
    D = -n.dotProduct(p);
    return *this;
  }

  //Calculates and returns distance between point and plane
  double distToPoint(Vector3 pos)
  {
    return n.dotProduct(pos) + D;
  }
};

/////////////////////////////////////
#endif //PLANE_HPP_
